<?php
Header ("Content-type: image/png"); // Browserinhalt image/png ! keine textausg
class field {
    public $im;                        // Variablendefinitionen der Klasse field
    public $ortx=0; public $orty=0;
    public $black; public $white; public $red; public $orange;
    function __construct($img) {          // Konstruktor - Initialisierung
             $this->im = $img;
             $this->black = imageColorAllocate($this->im, 0,0,0);
             $this->white = imageColorAllocate($this->im, 255,255,255);
             $this->green = imageColorAllocate($this->im, 0,255,0);
             $this->red = imageColorAllocate($this->im, 255,0,0);
             $this->orange = imageColorAllocate($this->im, 255,200,0);
             imagefill($this->im, 0,0,$this->green);
             }   // ende func field
}                // ende class field

class rect extends field {     // Vererbung: rect erbt von field
    function form() {          // neue Methode form
                               // F�llen eines Rechtecks 50*50 Pixel mit black
        imagefilledrectangle($this->im,
                             $this->ortx,
                             $this->orty,
                             $this->ortx+50,
                             $this->orty+50,
                             $this->black);
        }                      // ende func form

    function go($newx, $newy) {// Positionieren der Rechtecke
        $this->ortx=$newx;
        $this->orty=$newy;
        $this->form();
        }    // ende func go

}            // ende class rect

class ball extends rect {      // Vererbung: ball erbt von rect
    function form() {          // Methode form - hier f�r die Klasse ball
                               // jetzt einfarbige Kreise 50*50 Pixel mit red
        imagearc($this->im,
                 $this->ortx,
                 $this->orty, 50, 50, 0, 360,
                 $this->red);
        imagefilltoborder($this->im,          // keine andersfarbigen R�nder
                          $this->ortx,
                          $this->orty,
                          $this->red,
                          $this->red);
        }    // ende func form
}            // ende class ball

$image = imageCreate(400, 400);
$rect1 = new rect($image);      // neue Instanz rect - dadurch auch aufruf
                                // des Konstruktors von field!
$rect1->go(200,50);             // Rechteck oben Rechts
$rect1->go(50,200);             // Rechteck unten Rechts
$ball1 = new ball($image);      // neue Instanz ball

for ($x=0, $y=0; $x<400, $y<400; $x+=60, $y+=50) {
    $ball1->go($x, $y);         // B�lle mit Methode go zeichnen
    }

ImagePng($image);               // �bergabe des Bildes an Browser
ImageDestroy($image);           // L�schen der Bilddaten aus Arbeitsspeicher
?>







