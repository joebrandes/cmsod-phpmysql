﻿<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<?php
echo "Hallo ";
// viel besser mit htmlspecialchars:
echo htmlspecialchars ($_GET["name"]);
// echo $_GET["name"];
echo ". Sie sind ";
echo htmlspecialchars ($_GET["alter"]), " ";
echo "Jahre alt.";

echo "<br />";
echo $_SERVER['PHP_SELF']; // Pfad und Name der Datei selbst!
?>
</body>
</html>


