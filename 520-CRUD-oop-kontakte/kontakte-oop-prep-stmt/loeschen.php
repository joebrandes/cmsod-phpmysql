<?php
	require_once 'db.inc.php';
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Löschen - Kontaktverwaltung</title>
</head>

<body>
<?php
// Falls keine id per GET übergeben worden ist,
if (!isset($_GET['id'])) {
	// dann bitte einfach direkt wieder auf index.php
    header('Location: index.php');

// ansonsten, falls das POST-Feld ok nicht übergeben wurde
} elseif (!isset($_POST['ok'])) {
	// setze Variable id auf Ganzzahl von per GET übergebene Variable id
    $id = (int)$_GET['id'];
	// Und wieder: try/catch Blöcke zur Fehler-/Ausnahmebehandlung
    try {
		// Prepared Statement: SQL-Select String für Datensatz mit GET-id
		if ($stmt = $mysqli->prepare('SELECT id, vorname, nachname, email FROM kontakt WHERE id=?')) {
			// Binde GET-id (als integer) an Prepared Statement
			$stmt->bind_param("i",  $id);
			// Führe SQL-Select aus - Ergebnis für Datensatz liegt dann vor
			$stmt->execute();
			// Binde die folgenden Variablen an die Ergebnisse
			$stmt->bind_result($id, $vorname, $nachname, $email);
			// Hole uns die Ergebnisse
			$stmt->fetch();
		// Meldung / Feedback zum Löschdatensatz ausgeben
		// hier wird jetzt verstecktes Form-Feld ok genutzt (type hidden)!
		printf('Wollen Sie wirklich den Datensatz Nr. %s (%s %s, %s) löschen?<br />' . 
               '<form method="post" action=""><input type="hidden" name="ok" value="true" /><input type="submit" value="L&ouml;schen" /></form>', 
               (int) $id, 
               htmlspecialchars($vorname), 
               htmlspecialchars($nachname), 
               htmlspecialchars($email));

		} else {  // Falls es Fehler bei Prep-Stmt im ersten if gab:
			echo 'Fehler: ' . htmlspecialchars($mysqli->error) . '!</td></tr>';
		}

    } catch (Exception $ex) {  // catch zum try/catch-Block
		echo 'Fehler!';
	}  // Ende catch-Teil
	
	} else {
		// Variable id aus Ganzzahl der per GET übergebenen Variable id
		$id = (int)$_GET['id'];
		// try/catch again
		try {
			// Prepared Statemen SQL-String Delete mit Platzhalter (?) bei where id
			if ($stmt = $mysqli->prepare("DELETE FROM kontakt WHERE id=?")) {
			// Platzhalter muss gebunden werden
			$stmt->bind_param("i",  $id);
			// SQL ausführen
			$stmt->execute();
			// Objekte zurückgeben
			$stmt->close(); 
			$mysqli->close();
			// Ausgabe / Quittung
			echo 'Datensatz gelöscht!';
			} else {
				echo 'Fehler: ' . htmlspecialchars($mysqli->error) . '!</td></tr>';
			}
		} catch (Exception $ex) {
			echo 'Fehler!';
		}
	}
?>
<p><a href="index.php">Zur Startseite</a></p>
</body>
</html>