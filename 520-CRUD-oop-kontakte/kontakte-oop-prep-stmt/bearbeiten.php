<?php
	require_once 'db.inc.php';
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Bearbeiten - Kontaktverwaltung</title>
</head>

<body>
<?php
// Falls keine id per GET übergeben worden ist,
if (!isset($_GET['id'])) {
	// dann bitte einfach direkt wieder auf index.php
    header('Location: index.php');

// ansonsten, "kleine" Validierung Formular:
// vorname, nachname, email müssen alle per POST übergeben sein!	
} elseif (isset($_POST['vorname']) && isset($_POST['nachname']) && isset($_POST['email'])) {
    try {
		// Prepared Statement SQL-Befehl Update mit Platzhaltern
		$stmt = $mysqli->prepare('UPDATE kontakt SET vorname=?, nachname=?, email=? WHERE id=?');
		// Platzhalter binden mit 3 Strings und 1 integer Wert
		$stmt->bind_param('sssi', $_POST['vorname'], $_POST['nachname'], $_POST['email'], $id);
		// per GET übergebene hier wieder (siehe auch unten auf Variable id
		$id = (int)$_GET['id'];	
		// SQL-Update ausführen und Meldung(en) erzeugen
		if ($stmt->execute()) {
			echo 'Kontakt aktualisiert!';
		  } else {
			echo 'Fehler: ' . htmlspecialchars($mysqli->error) . '!</td></tr>';
		  }
		// Statement Objekt zurückgeben  
		$stmt->close();
		} catch (Exception $ex) {
			echo 'Fehler!';
		}
			// ende von elseif
} else { 	// Falls keine POST-Variablen
	// Variable id auf per GET übergebene Variable id setzen
    $id = (int)$_GET['id'];
    try {	// und wieder try/catch Block - Beachten Bracket endet nicht vor HTML-Code!
		// Prepared Statement: mit Select die Daten für den gewünschten Datensatz auslesen
		// auch hier wird die geschweifte Klammer (Bracket) erst nach dem HTML-Code geschlossen!
		if ($stmt = $mysqli->prepare("SELECT id, vorname, nachname, email FROM kontakt where id=?")) {
			// Variable id an Prep Stmt Select binden
			$stmt->bind_param("i",  $id);
			// SQL ausführen
			$stmt->execute();
			// Ergebnisse an gewünschte Variablen binden
			$stmt->bind_result($id, $vorname, $nachname, $email);
			// die Variablen holen
			$stmt->fetch();
			// es folgt das HTML-Formular... hier mit eingefügten Werten aus Select...
?>
<form method="post" action="">
<table>
<thead>
	<tr>
		<th>Information</th> <th>Wert</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Vorname</td> 
		<td><input type="text" name="vorname" value="<?php  echo htmlspecialchars($vorname); ?>" /></td>
	</tr>
	<tr>
		<td>Nachname</td>
		<td><input type="text" name="nachname" value="<?php echo htmlspecialchars($nachname); ?>" /></td>
	</tr>
	<tr>
		<td>E-Mail</td>
		<td><input type="text" name="email" value="<?php echo htmlspecialchars($email); ?>" /></td>
	</tr>
</tbody>
</table>
<input type="submit" value="Kontakt aktualisieren" />
</form>

<?php  
		} else {
			echo 'Fehler: ' . htmlspecialchars($mysqli->error) . '!</td></tr>';
		}
		$mysqli->close();
		} catch (Exception $ex) {
			echo 'Fehler!';
		}
	}
?>
<p><a href="index.php">Zur Startseite</a></p>
</body>
</html>