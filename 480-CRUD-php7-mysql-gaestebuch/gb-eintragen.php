<html>
<head>
  <title>G&auml;stebuch</title>
</head>
<body>
<h1>G&auml;stebuch</h1>
<?php
  if (isset($_POST["Name"]) &&
      isset($_POST["Email"]) &&
      isset($_POST["Ueberschrift"]) &&
      isset($_POST["Kommentar"])) {
    if ($db = mysqli_connect("localhost", "root", "", "PHP")) {
      $sql = "INSERT INTO gaestebuch 
        (ueberschrift, 
         eintrag,
         autor,
         email) 
        VALUES (?, ?, ?, ?)";
      $kommando = mysqli_prepare($db, $sql);
      mysqli_stmt_bind_param($kommando, "ssss", 
        $_POST["Ueberschrift"], 
        $_POST["Kommentar"],
        $_POST["Name"], 
        $_POST["Email"]);
      if (mysqli_stmt_execute($kommando)) {
        $id = mysqli_insert_id($db);
        echo "Eintrag hinzugef&uuml;gt. 
             <a href=\"gb-edit.php?id=$id\">Bearbeiten</a>";
      } else {
        echo "Fehler: " . mysqli_error($db) . "!";
      }
      mysqli_close($db);
    } else {
      echo "Fehler: " . mysqli_connect_error() . "!";
    }
  }
?>
<form method="post">
Name <input type="text" name="Name" /><br />
E-Mail-Adresse <input type="text" name="Email" /><br />
&Uuml;berschrift <input type="text" name="Ueberschrift" /><br />
Kommentar
<textarea cols="70" rows="10" name="Kommentar"></textarea><br />
<input type="submit" name="Submit" value="Eintragen" />
</form>
</body>
</html>
