<?php
  require_once 'db.inc.php';
?>
<html>
<head>
<title>Kontaktverwaltung</title>
<body>
<?php
  if (!isset($_GET['id'])) {
    header('Location: index.php');
  } elseif (isset($_POST['vorname']) && isset($_POST['nachname']) && isset($_POST['email'])) {
    try {
      $db = Singleton::holeVerbindung();
      $db->select_db('kontakte');
      $sql = $db->prepare('UPDATE kontakt SET vorname=?, nachname=?, email=? WHERE id=?');
      $sql->bind_param('sssi', $_POST['vorname'], $_POST['nachname'], $_POST['email'], $id);
      $id = (int)$_GET['id'];
      if ($sql->execute()) {
        echo 'Kontakt aktualisiert!';
      } else {
        echo 'Fehler: ' . htmlspecialchars($db->error) . '!</td></tr>';
      }
      $db->close();
    } catch (Exception $ex) {
      echo 'Fehler!';
    }
  } else {
    $id = (int)$_GET['id'];
    try {
      $db = Singleton::holeVerbindung();
      $db->select_db('kontakte');
      if (($ergebnis = $db->query('SELECT * FROM kontakt WHERE id=' . $id)) && 
          $zeile = $ergebnis->fetch_object()) {
        $vorname = $zeile->vorname;
        $nachname = $zeile->nachname;
        $email = $zeile->email;
?>
<form method="post" action="">
<table>
<thead>
<tr><th>Information</th><th>Wert</th></tr>
</thead>
<tbody>
<tr><td>Vorname</td><td><input type="text" name="vorname" value="<?php
  echo htmlspecialchars($vorname);
?>" /></td></tr>
<tr><td>Nachname</td><td><input type="text" name="nachname" value="<?php
  echo htmlspecialchars($nachname);
?>" /></td></tr>
<tr><td>E-Mail</td><td><input type="text" name="email" value="<?php
  echo htmlspecialchars($email);
?>" /></td></tr>
</tbody>
</table>
<input type="submit" value="Kontakt aktualisieren" />
</form>
<?php  
      } else {
        echo 'Fehler: ' . htmlspecialchars($db->error) . '!</td></tr>';
      }
      $db->close();
    } catch (Exception $ex) {
      echo 'Fehler!';
    }
  }
?>
<p><a href="index.php">Zur Startseite</a></p>
</body>
</html>