<?php
// einfaches nummerische Array:
$arr01 = array ("HTML", "CSS", "JS");
echo $arr01[1], "<br>";
$arr01[] = "Neuer Wert in Array";
echo $arr01[3], "<br>";
// Frage: wie sieht mein Array aus?
// 2 Funcs: print_r und var_dump
echo "<pre>";
print_r ($arr01);
var_dump ($arr01);
// var_dump ($_SERVER);
echo "</pre>";

// neues assoziatives Array
$arr02 = array ( 
	"Vorname"  => "Max", 
	"Nachname" => "Mustermann",
	"PLZ"      => "01234",
	"Ort"      => "Musterdorf"   
	);
// echo $arr02["PLZ"];
//echo "<pre>"; print_r ($arr02); echo "</pre>";
// Ort neu in Array
$arr02["Ort"] = "Musterhausen";
// echo "<pre>"; print_r ($arr02); echo "</pre>";
// Neuen Index aufnehmen: Telefonnummer
$arr02["Telefonnummer"] = "+49 (531) 1234-56";
echo "<pre>"; 
var_dump ($arr02); 
echo "</pre>";	




// neue nummerisches Array für einache Anwendung von list():
$arr03 = array ("Mäxchen", "Musterfrau", "01234", "Musterdörfchen", "0531 53855555");

list ($vorname, $nachname, $plz, $ort, $telnummer) = $arr03;

echo "$vorname $nachname lebt in $plz $ort und hat die Telefonnummer $telnummer!";

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
?>