<html>
<head>
  <title>G&auml;stebuch</title>
</head>
<body>
<h1>G&auml;stebuch</h1>
<?php
  if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
    if (isset($_GET["ok"])) {
      if ($db = mysqli_connect("localhost", "root", "", "PHP")) {
        $id = mysqli_escape_string($db, $_GET["id"]);
        $sql = "DELETE FROM gaestebuch WHERE id=$id";
        if (mysqli_query($db, $sql)) {
          echo "<p>Eintrag gel&ouml;scht.</p>
                <p><a href=\"gb-admin.php\">Zur&uuml;ck zur &Uuml;bersicht
                    </a></p>";
        } else {
          echo "Fehler: " . mysqli_error($db) . "!";
        }
        mysqli_close($db);
      } else {
        echo "Fehler: " . mysqli_connect_error() . "!";
      }
    } else {
      printf("<a href=\"gb-admin.php?id=%s&ok=1\">Wirklich l&ouml;schen?
                </a>", urlencode($_GET["id"]));
    }
  } else {
    if ($db = mysqli_connect("localhost", "root", "", "PHP")) {
      $sql = "SELECT * FROM gaestebuch ORDER BY datum DESC";
      $ergebnis = mysqli_query($db, $sql);
      while ($zeile = mysqli_fetch_object($ergebnis)) {
        printf("<p><b><a href=\"gb-admin.php?id=%s\">Diesen Eintrag
                l&ouml;schen</a> - <a href=\"gb-edit.php?id=%s\">
                 Diesen Eintrag &auml;ndern
                       </a></b></p>
          <p><a href=\"mailto:%s\">%s</a> schrieb am/um %s:</p>
          <h3>%s</h3><p>%s</p><hr noshade=\"noshade\" />", 
          urlencode($zeile->id),
          urlencode($zeile->id),
          htmlspecialchars($zeile->email),
          htmlspecialchars($zeile->autor),
          htmlspecialchars(date("d.m.Y, H:i", strtotime($zeile->datum))),
          htmlspecialchars($zeile->ueberschrift),
          nl2br(htmlspecialchars($zeile->eintrag))
        );
      }
      mysqli_close($db);
    } else {
      echo "Fehler: " . mysqli_connect_error() . "!";
    }
  }
?>
</body>
</html>
