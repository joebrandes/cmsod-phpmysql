<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="kal_styling.css">
    <title>Kalender</title>
</head>
<body>

<?php
require("kal_func.php");

// Language Strings auf/für Windows Systeme
// https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2008/39cwe7zf(v=vs.90)
// setlocale(LC_TIME, "dutch");  // Dutch - Holländisch
// Alternativen: german / deu, french / fra, italian / ita, danish / dan, spanish / esp
setlocale(LC_TIME, "german");  // Hier die Syntax für Windows-Serverumgebung!

// Analyse für Umgebung:
// $loc_de = setlocale(LC_ALL, 'de_DE@euro', 'de_DE', 'deu_deu');
// echo $loc_de; die ("<br>schluss");       // result for preferred locale on Windows System: German_Germany.1252

echo "<table>";           // Beginn der Tabelle
                   // 1.Zeile der Kalendertabelle
echo "<tr class=\"rows\">";                // Erstellen 1.Zeile

/* die alte Skriptdarstellung in einem PHP 3 Fachbuch ;-)
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 1,1, 2002)),"</b></td>";
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 2,1, 2002)),"</b></td>";
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 3,1, 2002)),"</b></td>";
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 4,1, 2002)),"</b></td>";
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 5,1, 2002)),"</b></td>";
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 6,1, 2002)),"</b></td>";
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 7,1, 2002)),"</b></td>";
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 8,1, 2002)),"</b></td>";
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 9,1, 2002)),"</b></td>";
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 10,1, 2002)),"</b></td>";
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 11,1, 2002)),"</b></td>";
echo "<td width=80><b>",strftime('%b', mktime (0,0,0, 12,1, 2002)),"</b></td>";
*/

// die 12 Monate in erster Zeile der Tabelle mit Schleife ausgeben:
// Problem mit UTF-8 encoding für die func strftime auf Windows Systemen:
// https://stackoverflow.com/questions/12680716/german-umlauts-in-strftime-date-formatting-correct-utf-8-encoding

for ($i = 1; $i <= 12; $i++) {
    $monGerman = strftime('%B', mktime (0, 0, 0, $i, 1, 2002));
    $monGermanUTF8 = utf8_encode($monGerman);
	echo "<th>", $monGermanUTF8 , "</th>";
}

echo "</tr>";     // Ende der 1.Zeile der Kalendertabelle

// Hilfsfunktion für Ausgabe Monate
function mon_out($mon, $z) {
    while (monx($z)==$mon) {
        if (tagnum($z)==0) {          // Ausgabe der Sonntage
            echo "<span class=\"sundays\">";
            echo kwtagx($z), " ", utf8_encode(tagnamx($z)), " ", tagmonx($z);
            echo "</span>";
            }
        else {                        // Ausgabe der Standardtage
            echo "<span class=\"workdays\">";
            echo utf8_encode(tagnamx($z)), " ", tagmonx($z);
            echo "</span>";
            }
        echo "<br>";
        $z+=1;
        }
    return $z;
    }


// Monate ausgeben:
$tag=1;           // Tageszähler initialisieren
    // Start der folgenden Zeile - in jeder Datenzelle td
    // sind die Daten des jeweiligen Monats
echo "<tr>";

for ($mon=1; $mon<13; $mon++) {     // die 12 Monate durchspielen
    echo "<td>";
    $tag=mon_out($mon, $tag);       // func mon_out aufrufen - Monat ausgeben
    echo "</td>";
}
echo "</tr> </table>";          // Tabellenende


$day=45;                               // Erklärungen der genutzten Funktionen
// bei falschen Ausgaben/Ausgabezeichen bitte wieder mit utf8_encode() arbeiten!
echo "<p>Die genutzten Funktionen:</p>";
echo "func monx($day) = ", monx($day), "<br>";
echo "func tagmonx($day) = ", tagmonx($day), "<br>";
echo "func tagnamx($day) = ", tagnamx($day), "<br>";
echo "func tagnum($day) = ", tagnum($day), "<br>";
echo "func kwtagx($day) = ", kwtagx($day), "<br>";
echo "func datx($day) = ", datx($day), "<br>";

?>

</body>
</html>
