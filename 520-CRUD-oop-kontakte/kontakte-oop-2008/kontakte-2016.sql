--
-- Datenbank: `kontakte`
--
CREATE DATABASE IF NOT EXISTS `kontakte` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kontakte`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kontakt`
--

DROP TABLE IF EXISTS `kontakt`;
CREATE TABLE IF NOT EXISTS `kontakt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vorname` varchar(50) NOT NULL,
  `nachname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `kontakt`
--

INSERT INTO `kontakt` (`id`, `vorname`, `nachname`, `email`) VALUES
(1, 'Joe', 'Test', 'dummy@aol.com'),
(2, 'Max', 'Mustermann', 'muster@aol.com'),
(4, 'Joe', 'Freitag', 'dummy3@aol.com');