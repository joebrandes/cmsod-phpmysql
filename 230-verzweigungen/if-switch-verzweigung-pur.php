﻿<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<?php
// zwei Variablen definieren
$a = 10;
$b = 12;

// Verzweigungen mit if - bitte Manual beachten!
if ($a > $b) {
  echo "a ist größer als b";
} else {
  echo "a ist NICHT größer als b";
}

echo "<br>\n";

// Verzweigen / Abarbeiten mit "Schalter" switch:
$i = 2;
switch ($i) {
    case 0:
        echo "i gleich 0";
        break;
    case 1:
        echo "i gleich 1";
        break;
    case 2:
        echo "i gleich 2";
        break;
    default:
       echo "i ist nicht gleich 0, 1 oder 2";
}



?> 
</body>
</html>