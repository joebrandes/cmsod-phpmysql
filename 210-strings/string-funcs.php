﻿<!doctype html>
<html lang="de">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<?php
echo "<p>String-Funktion: addslashes()
<br> (Escapen von Zeichenfolgen: einf&uuml;gen von Backslashes vor
Sonderzeichen in Strings; Anm: z.B. Problem bei Datenbanken)<br>";
$str = "Joe's Lamp-Kurs";    // Anführungszeichen kann von DB als Stringende
                             //angesehen werden!
echo addslashes($str);

echo "<p>String-Funktion: stripslashes()
<br> (Entfernen der eingef&uuml;gten Backslashes)<br>";
$str = "Joe's Lamp-Kurs";    // Anführungs zeichen kann von DB als Stringende
                             //angesehen werden!
$str1 = addslashes($str);
echo "Mit Slashes: ".$str1;
echo "<br>Ohne Slashes: ". stripslashes($str1);

echo "<p>String-Funktion: explode()
<br> (Zeichenkette zerteilen)<br>";
$str = "Diese Zeichenkette soll zerteilt werden!";
echo "Original: ". $str;
echo "<br>";
$arr = explode(" ", $str);    // Bei Leerzeichen auseinandernehmen
foreach ($arr as $elem) {
    echo $elem . "<br>";
}

echo "<p>String-Funktion: implode()
<br> (Zeichenkette zusammensetzen)<br>";
$str = "Diese Zeichenkette wird zerteilt und wieder zusammengesetzt!";
echo "Original: ". $str;
echo "<br>";
$arr = explode(" ", $str);    // Bei Leerzeichen auseinandernehme
echo "<br>dann auseinanandergenommen s.o.
<br>und mit F&uuml;llzeichen wieder zusammengesetzt:<br>";
$str1 = implode(":", $arr);
echo $str1;

echo "<p>String-Funktion: substr()
<br> (Teilstring suchen und &uuml;bergeben)<br>";
$pos=10;
$length=12;
$str = "In dieser Zeichenkette soll gesucht werden!";
echo "Original: ". $str;
echo "<br>Jetzt an $pos. Stelle gehen und $length Zeichen nehmen!<br>
ergibt:<br>";
$str1 = substr($str, $pos, $length);    // an pos mit length Zeichen
echo $str1, "<br>";

$satz = "außerhalb @@bitteersetzen@@";
$erg  = str_replace("@@bitteersetzen@@", "des Planeten", $satz);
echo "Vorher: $satz, nachher: $erg";

echo "<br />\n";

$text = "Schöne Grüße";
$sonder = array("ö", "ä", "ü", "ß", "Ö", "Ü", "Ä");
$ohne = array("oe", "ae", "ue", "ss", "Oe", "Ue", "Ae");
echo str_replace($sonder, $ohne, $text);

?>


	
</body>
</html>