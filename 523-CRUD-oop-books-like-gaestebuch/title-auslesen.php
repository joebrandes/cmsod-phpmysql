<html>
<head>
  <title>books - title</title>
</head>
<body>
<h1>Bücherliste</h1>
<?php
  if ($db = mysqli_connect("localhost", "root", "", "books2")) {
    $sql = "SELECT * FROM title";
    $ergebnis = mysqli_query($db, $sql);

	// print_r ($ergebnis); die ("Tschüss");

    while ($zeile = mysqli_fetch_object($ergebnis)) {

	  // print_r ($zeile); die ("Tschüss");

      printf("<h3>%s</h3><p>%s<br>%s</p><hr>",
        htmlspecialchars($zeile->title),
        htmlspecialchars($zeile->publisherID),
        htmlspecialchars($zeile->year)
      );
    }
    mysqli_close($db);
  } else {
    echo "Fehler: " . mysqli_connect_error() . "!";
  }
?>
</body>
</html>
