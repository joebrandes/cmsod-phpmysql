﻿<!doctype html>
<html lang="de">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	
<?php	

echo "<p>String-Funktion: print()
<br> (akzeptiert nur ein Argument)<br>";
$a="Vorname: <br>";
$b="Name: <br>";
$d="Ort: <br>";
echo $a, $b;
print $d;

echo "<p>String-Funktion: sprintf()
<br> (akzeptiert mehrere Argumente - kann formatieren (Zahlenformate!)<br>
&Uuml;bergabe an String!)<br>";
$a="Vorname: <br>";
$b="Name: <br>";
$erg= sprintf("%s%s", $a, $b); //%s gibt Zeichenkette aus
print $erg;

// Hinweis: die Übungen zu sprintf() ergeben dann später mit
// den Datenbanktechniken ihre besondere Bedeutung!

// siehe Hilfe sprintf aus php.net (siehe Alt + F1)

$anzahl = 5;
$ort    = 'Baum';
$format = 'Der %2$s enthält %1$d Affen.
           %1$d Affen sind ziemlich viel für einen %2$s.';
echo sprintf($format, $anzahl, $ort);

// Hinweis: eine sehr wichtige Grundfunktion zum Entschärfen 
// von Eingabeformularen
echo "<p>String-Funktion: htmlspecialchars()
<br> (Sonderzeichen < und > verlieren ihre HTML-Bedeutung!)<br>";
$str = "<html> <head> <title>String-Funktionen f&uuml;r Ausgaben</title>
<meta name='author' content='Joe Brandes'> <meta name='generator'
content='PHP Coder'> </head> <body> Testseite </body> </html>" ;
echo "So wird der Text-String als HTML-Code interpretiert: <br>\n";
echo $str;
echo "<br>so als einfacher String:<br>";
echo htmlspecialchars($str);


// Hinweis: zum Erzeugen von URL-fähigen Zeichenstrings nötig
echo "<p>String-Funktion: rawurlencode()
<br> (Sonderzeichen f&uuml;r URL erzeugen!)<br>";
$str="Dieser String soll in einer URL verwendet & via GET übertragen werden";
echo "Das ist der Originalstring:<br>";
echo $str;  //mit Leerzeichen, & und Umlauten
echo "<br>Das ist der ver&auml;nderte Originalstring:<br>";
echo rawurlencode($str);

// Hinweis: die manuellen <br> hier mit Automatik bei 
// über Zeilen verteilten Strings
echo "<p>String-Funktion: nl2br()
<br> (ersetzt in Zeichenketten Zeilenumbr&uuml;che mittels br!)<br>";
$str="Dieser String geht
&uuml;ber 2 Zeilen";
echo "Ohne func nl2br: $str<br>Mit func nl2br: ";
echo nl2br($str);


// Hinweis: Suchmaschine "light"
echo "<p>String-Funktion: get_meta_tags()
<br> (liest meta-infos aus einem html-dok!)<br>";
//$arr=get_meta_tags("http://www.pcsystembetreuer.de");
//$arr=get_meta_tags("http://www.vhs-braunschweig.de");
$arr=get_meta_tags("get_meta_tags.html");
// $arr=get_meta_tags("http://www.spiegel.de");
foreach ($arr as $key=>$elem) {
    echo $key," => ", $elem,"<br>";
}
// Anzeige / Übersicht meiner Arrays mit Testfunktionen:
echo "<pre>";
print_r($arr);
echo "</pre>";

echo "<pre>";
var_dump($arr);
echo "</pre> <br />\n";

?>	
	
</body>
</html>