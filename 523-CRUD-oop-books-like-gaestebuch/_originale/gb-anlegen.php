<?php
  if ($db = mysqli_connect("localhost", "root", "", "PHP")) {
    $sql = "CREATE TABLE gaestebuch (
      id INT AUTO_INCREMENT PRIMARY KEY, 
      ueberschrift VARCHAR(1000), 
      eintrag VARCHAR(8000),
      autor VARCHAR(50),
      email VARCHAR(100),
      datum TIMESTAMP
    )";
    if (mysqli_query($db, $sql)) {
      echo "Tabelle angelegt.<br />";
    } else {
      echo "Fehler: " . mysqli_error($db) . "!";
    }
    mysqli_close($db);
  } else {
    echo "Fehler: " . mysqli_connect_error() . "!";
  }
?>
