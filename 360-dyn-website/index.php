<?php
  include 'menu_builder.php';
  $dyninhalt = isset($_GET['dyninhalt']) ? $_GET['dyninhalt'] : 'index';

  // Konfiguration:
  $articlefolder = 'articles/'; // Declare Articlefolder
  $template = 'css4you'; // Auswahl css4you, artificial
  // Parameter Datum und Artikel erzeugen
  $datum = date('d.m.Y'); // Datum aktuell
  $article = $articlefolder.$dyninhalt.'.html';  // Artikel dem Html-Dok zuordnen
  
  // Abfangen falls kein Artikel vorhanden
  if($dyninhalt && file_exists($articlefolder.$dyninhalt.'.html'))
  {
    // Artikel und Sektion einlesen
    $inhalt = file_get_contents($article);
    $sektion = $dyninhalt;
  }
  else // startseite ohne parameter dyninhalt
  {
    // Artikel und Sektion einlesen
    $inhalt = file_get_contents($articlefolder.'index.html');
	$sektion = 'index';
  }
	
	// template einladen
    $output_html = implode('',file('templates/'.$template.'/index.tpl'));  
    // Ausgabe kreieren
    $output_html = str_replace('@@bereich@@', $sektion, $output_html);
    $output_html = str_replace('@@content@@', $inhalt ,$output_html);
    $output_html = str_replace('@@datum@@', $datum, $output_html);
    $output_html = str_replace('@@dynmenu@@', $menustr, $output_html);    
    echo $output_html;

?>