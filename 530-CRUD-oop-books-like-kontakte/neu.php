<?php
	require_once 'db.inc.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<title>Neu - Buchverwaltung</title>
</head>

<body>
<?php
// Formular validieren: (hier sehr einfach / grob)
// die Variablen title, publisherID, year müssen
// alle per Methode Post übergeben worden sein und
// alle den Datentyp String besitzen und die year darf nicht leer sein!
if (isset($_POST['title']) && is_string($_POST['title']) && 
    isset($_POST['publisherID']) && is_string($_POST['publisherID']) && 
    isset($_POST['year']) && is_string($_POST['year']) && !empty($_POST['year'])) {
    
	// wieder so eine try/catch Konstruktion zur Fehler-/Ausnahmebehandlung
	try {
		// Prepared Statement: Einfügen-SQL-String mit Platzhaltern (?)
		$stmt = $mysqli->prepare('INSERT INTO title (title, publisherID, year) VALUES (?, ?, ?)');
		// im Prepared Statement Objekt die übergebenen POST-Inhalte binden
		$stmt->bind_param('ssi', $_POST['title'], $_POST['publisherID'], $_POST['year']);
		// SQL Einfügen ausführen in IF-Bedingung, also: bei Erfolg
		if ($stmt->execute()) {
			// spezielle Methode des DB-Konnektions-Objekts nutzen
			// Methode insert_id() ermittelt uns die neu erzeugte id des Datensatz
			$id = $mysqli->insert_id;
			// Infos zu neuem Datensatz (id) ausgeben, falls alles funktioniert hat
			echo 'Kontakt eingetragen (ID: ' . $id . ')!';
			} else {
				echo 'Fehler: ' . htmlspecialchars($db->error) . '!</td></tr>';
			}
			
		// alle Objekte wieder freigeben	
		$stmt->close();
		$mysqli->close();
		
		// hier wieder der catch-Teil zur try/catch Technik
		} catch (Exception $ex) {
			echo 'Fehler!';
		}
	}	// es folgt ein leeres HTML-Formular für neue Datensätze / Kontakte
?>
<form method="post" action="">
<table>
<thead>
	<tr> <th>Information</th> <th>Wert</th> </tr>
</thead>
<tbody>
	<tr>
		<td>title</td> <td><input type="text" name="title" /></td>
	</tr>
	<tr>
		<td>publisherID</td> <td><input type="text" name="publisherID" /></td>
	</tr>
	<tr>
		<td>year</td> <td><input type="text" name="year" /></td>
	</tr>
</tbody>
</table>
	<input type="submit" value="Buch anlegen" />
</form>
<p> <a href="index.php">Zur Startseite</a> </p>
</body>
</html>