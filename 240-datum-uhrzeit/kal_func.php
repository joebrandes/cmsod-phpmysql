<?php
// kal_func.php3
// Hilfs-Funktionen zur Datumsberechnung - noch aus PHP3-Tagen ;-)

// Monat als Zahl aus Tag des Jahres
// Beispiel: monx(45) = 2
function monx($x) {
    $heute= mktime (0,0,0, date("m"),date("d"), date ("Y"));
    $tag_heute= (int)strftime("%j", $heute);
    $tag_x= mktime (0,0,0, date("m"),date("d")-$tag_heute+$x, date ("Y"));
    $x_monat= (int)strftime("%m", $tag_x);
    return $x_monat;
  }

// Tag im Monat aus Tag des Jahres
// Beispiel: tagmonx(45) = 14
function tagmonx($x) {
    $heute= mktime (0,0,0, date("m"),date("d"), date ("Y"));
    $tag_heute= (int)strftime("%j", $heute);
    $tag_x= mktime (0,0,0, date("m"),date("d")-$tag_heute+$x, date ("Y"));
    $x_tagmon= (int)strftime("%d", $tag_x);
    return $x_tagmon;
}


// Tag als Kurzname aus Tag des Jahres
// Beispiel: tagnamx(45) = So
function tagnamx($x) {
    $heute= mktime (0,0,0, date("m"),date("d"), date ("Y"));
    $tag_heute= (int)strftime("%j", $heute);
    $tag_x= mktime (0,0,0, date("m"),date("d")-$tag_heute+$x, date ("Y"));
    $x_wotaname= strftime("%a", $tag_x);
    return $x_wotaname;
}

// Wochentag als Zahl aus Tag des Jahres (So=0, Mo=1, ...)
// Beispiel: tagnum(45) = 0
function tagnum($x) {
$heute= mktime (0,0,0, date("m"),date("d"), date ("Y"));
$tag_heute= (int)strftime("%j", $heute);
$tag_x= mktime (0,0,0, date("m"),date("d")-$tag_heute+$x, date ("Y"));
$x_wotanum= strftime("%w", $tag_x);
return $x_wotanum;
}

// Kalenderwoche aus Tag des Jahres
// Beispiel: kwtagx(45) = 07
function kwtagx($x) {
    $heute= mktime (0,0,0, date("m"),date("d"), date ("Y"));
    $tag_heute= (int)strftime("%j", $heute);
    $tag_x= mktime (0,0,0, date("m"),date("d")-$tag_heute+$x, date ("Y"));
    $x_kw= strftime("%U", $tag_x);
    return $x_kw;
}

// Tag formatiert ausgeben aus Tag des Jahres
// Beispiel: datx(45) = 14 Feb 2021
function datx($x) {
  $heute= mktime (0,0,0, date("m"),date("d"), date ("Y"));
  $tag_heute= (int)strftime("%j", $heute);
  $tag_x= mktime (0,0,0, date("m"),date("d")-$tag_heute+$x, date ("Y"));
  $dat= strftime("%d %b %Y", $tag_x);
  return $dat;
}

