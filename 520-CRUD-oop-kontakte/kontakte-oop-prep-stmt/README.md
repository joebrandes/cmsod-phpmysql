# README


Status: 02.05.2016

Siehe auch Originalbeitrag auf pcsystembetreuer.de

Diese kleine Programmierübung soll die generellen Zugriffe per PHP / MySQLi Technik darstellen.

Hierzu sollen die Befehlsstrukturen mit **OOP / Prepared Statements** umgesetzt werden.

Die hier dargestellten Beispielskripte sind eine Zusammenfassung von PHP-Datenbank-Übungen, wie wir sie ab 2016 im Rahmen unserer **PHP & MySQL**-Seminaren für das Zertifikat "CMS Online Designer (VHS)" schulen.

## Datenbank NEWS

Quellen zu OOP-DB-Techniken:

Siehe auch:

*   Dr. Florence Maurice - PHP 5.5 und MySQL 5.6 - dpunkt Verlag 2014; Kapitel 11 PHP & MySQL - Kap.
    11.6 Prepared Statements - auf alles bestens vorbereitet

*   Beispiele php.net: https://www.php.net/manual/de/mysqli.prepare.php und Andere...

**Hier**: PHP & MySQL Bibliothek Datenbank "kontakte"
Christian Wenz; Internet magazin 02/2008 - MySQL mit PHP "Der Datenturbo"
Einführung in die Techniken OOP, mysqli, Prepared Statements

Zusammen ergibt sich die vorliegende **Kontakteverwaltung** mit den folgenden Techniken:

*   PHP mysqli mit OOP und Prepared Statement
*   Fehler- und Ausnahmenbehandlungen mit try/catch Blöcken
*   Einfachste Formularbehandlung
*   Konzentration auf Darstellung Grundoperation **CRUD** (Create, Read, Update, Delete)

## Übung:

Die notwendige Datenbank, Tabelle und Inhalte bereitstellen und dann die vorbereiteten Skripte nutzen.

### Datenbank:

Für die vorliegende Kontaktverwaltung benötigen Sie einen XAMPP und die Datenbank **phpmysql_kontakte**.

Sie können sich natürlich eine eigene DB erstellen und müssen dann im DB-Include-Skript die Infos anpassen.

Variante 1: mit phpMyAdmin eine Datenbank **phpmysql_kontakte** erstellen;
danach Tabelle importieren mittels ``tabelle-kontakt-erstellen.sql``

Variante 2: gesamte Datenbank **phpmysql_kontakte** inklusive Tabelle kontakt mit beiliegenden SQL-Dump ``db-phpmysql_kontakte-tabelle-kontakt-erstellen.sql`` in DB-Server importieren.

### Skripte analysieren und modifizieren

Die beteiligten PHP-Skripte wurden hierfür ausgiebig kommentiert.

*   ``db.inc.php``

    Datenbank konnektieren und mysqli-Objekt bereitstellen.

*   ``index.php``

    Aktuelle Kontaktverwaltung auslesen und anzeigen.

*   ``neu.php``

    Einen neuen Datensatz aufnehmen in Tabelle kontakt.

*   ``loeschen.php``

    Einen Datensatz nach Rückfrage aus Tabelle kontakt löschen.

*   ``bearbeiten.php``

    Einen Datensatz zum ändern / Aktualisieren bereitstellen und in Tabelle kontakt aktualisieren.





