<html>
<head>
  <title>G&auml;stebuch</title>
</head>
<body>
<h1>G&auml;stebuch</h1>
<?php
  // Hauptroutine per if "abfangen" mit DB-Connection:
  // mit Variable $db Zugriff auf DB-Datenbank Verbindung
  if ($db = mysqli_connect("localhost", "root", "", "PHP")) {
	  
	// SQL-String erstellen für Select:
	$sql = "SELECT * FROM gaestebuch ORDER BY datum DESC";
	// Abfrage an die Datenbank:
    $ergebnis = mysqli_query($db, $sql);
    
	// eigentliche Hauptroutine - Ausgabe der Datensätze
	// mit Hilfe einer While-Schleife (Abbruchbedingung am Anfang)
	while ($zeile = mysqli_fetch_object($ergebnis)) {
	  
	  // mit formatiertem printf / sprintf die Datensätze ausgeben:
      printf("<p><a href=\"mailto:%s\">%s</a> schrieb am/um %s:</p>
        <h3>%s</h3><p>%s</p><hr noshade=\"noshade\" />", 
        urlencode($zeile->email),
        htmlspecialchars($zeile->autor),
        htmlspecialchars(date("d.m.Y, H:i", strtotime($zeile->datum))),
        htmlspecialchars($zeile->ueberschrift),
        nl2br(htmlspecialchars($zeile->eintrag))
      );
	  
    }
	
	
    mysqli_close($db);
	
	
	
	
  } else {
    echo "Fehler: " . mysqli_connect_error() . "!";
  }
?>
</body>
</html>
