<?php
  
  if($_GET['fn'] && file_exists($_GET['fn']))
  {
    // Template in eine Variable laden
	// $test = file('template.htm');
	// echo gettype ($test); die ("Tschüss");
    $output_html = implode('', file('template.htm'));
	// echo gettype($output_html); die ("Tschüss");
	// echo $output_html; die ("bis hierhin und nicht weiter!");
    
    // CSV-Datei einlesen
    $pics = file($_GET['fn']);
	// echo "<pre>"; print_r($pics); echo "</pre>"; die ("bis hierhin und nicht weiter!");
    
    // Nummer des anzuzeigenden Bildes festlegen
	// hier berüchtigte Kurzschreibung:
    // $pic_nr = $_GET['nr'] ? $_GET['nr'] : 1;
	// hier die LANGE Schreibweise mit IF:
	if ($_GET['nr']) {
		$pic_nr = $_GET['nr'];
	} else {
		$pic_nr = 1;
	}
	
    
    // Bilddatensatz laden: filename (Dateiname) u. Beschreibung
    list($filename, $description) = explode(';',$pics[$pic_nr-1]);
    // neue Variablen testen:
	// echo $description; die ("<br>bis hierhin und nicht weiter!");
	
    // Vorwärts - Rückwärts
    $pprev = ($pic_nr <= 1) ? 1 : ($pic_nr - 1);
    $pnext = ($pic_nr >= count($pics)) ? count($pics) : ($pic_nr + 1);
    	
    // Picturelist - Bildliste am unteren Rand der Diashow
    $piclist = '';
    for($i=0; $i<count($pics); $i++)
    {
      $piclist .= '<a href="'.$_SERVER['PHP_SELF'].'?fn='.$_GET['fn'].'&nr='.($i+1).'">Bild '.($i+1).'</a>';
      if(($i+1) <> count($pics))
      {
        $piclist .= "&nbsp;&middot;&nbsp;\n";
		// $piclist .= '&nbsp;&euro;&nbsp;';
      }
    }
	
	$aktdatum = date ("Y");
    
    // Ausgabe
    $output_html = str_replace('<@prevlink@>', $_SERVER['PHP_SELF'].'?fn='.$_GET['fn'].'&nr='.$pprev, $output_html);
    $output_html = str_replace('<@nextlink@>', $_SERVER['PHP_SELF'].'?fn='.$_GET['fn'].'&nr='.$pnext, $output_html);
    $output_html = str_replace('<@picture@>', $filename, $output_html);
    $output_html = str_replace('<@number@>', $pic_nr, $output_html);
    $output_html = str_replace('<@piccount@>', count($pics), $output_html);
    $output_html = str_replace('<@description@>', $description, $output_html);
	// echo $output_html; die ("bis hierhin...");
	
    $output_html = str_replace('<@piclist@>', $piclist, $output_html);
    $output_html = str_replace('<@datum@>', $aktdatum, $output_html);	
    echo $output_html;
  }
  else
  {
    die('Ungültige Diashow!');
  }

?>