<?php
Header ("Content-type: image/png");

class field {           // Start der Definition durch Schl�sselwort class
                        // gesamte Klassendefinition in geschweiften Klammern
    public $im;                    // Vereinbarung der Klassenvariablen
    public $ortx = 0; public $orty = 0;
    public $black; public $white; public $red; public $orange;
    function __construct($img) {    // Konstruktor - (meistens zur Initialiserung)
                              // wird automatisch beim Erzeugen einer Instanz
                              // der Klasse ausgef�hrt

             $this->im = $img;  // Schl�sselwort this - Verweis auf Klassenvar
                                // oder Methode dieser Klasse
             $this->black = imageColorAllocate($this->im, 0,0,0);
             $this->white = imageColorAllocate($this->im, 255,255,255);
             $this->green = imageColorAllocate($this->im, 0,255,0);
             $this->red = imageColorAllocate($this->im, 255,0,0);
             $this->orange = imageColorAllocate($this->im, 255,200,0);

// int imagefill ( int im, int x, int y, int col)
// ImageFill() bewirkt das F�llen eines Bildes beginnend
// bei der Koordinate x, y (oben links ist 0, 0) mit der Farbe col im Bild im.
             imagefill($this->im, 0,0,$this->green);

             }   // ende des Konstruktors; fr�her: function field () {}
}                // ende class field

$image = imageCreate(400, 400);  // Erzeuge neues Bild
$field1 = new field($image);     // erzeuge neue Instanz / neues Objekt der Klasse field
								 // mit einem Konstruktor!
ImagePng($image);                // �bergabe des Bildes an den Browser
ImageDestroy($image);            // L�schen des Bildes aus dem Arbeitsspeicher


?>


