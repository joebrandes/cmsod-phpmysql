CRUD mit DB News von Florence Maurice
=====================================

Mit den Daten aus dem Buch von Florence Maurice:

*   mit *PhpMyAdmin* die Datenbank **NEWS** erstellen

*   mit ``01-tabelle_aktuell_erstellen.sql`` Import die Tabelle **AKTUELL** erstellen

*   mit ``02-tabelle_aktuell_daten.sql`` Import Daten in Tabelle **AKTUELL** einfügen

*   Datenbank Konnektion in ``db_daten_aktuell.php`` checken!


Alternative: Gesamtlösung - alles in einem SQL-Import:

*   mit *PhpMyAdmin* den Import mit ``ZZ-db-news_tabelle_aktuell_inkl_daten.sql``
