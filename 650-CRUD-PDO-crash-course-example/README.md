#   README

Diese Code-Beispielreihe bietet sich für **professionelle** Nachbereitung
und **fortgeschrittenes Training** an!


##  Quellle

https://github.com/thecodeholic/php-crash-course-2020/tree/final-version

Kapitel ``14_product_crud``

##  Techniken

*   PDO für Datenbankzugriffe
*   Entwurfsmuster MVC (Mode - View - Controller)
