<?php
// einfaches nummerische Array:
$cmsod = array (
				"HTML & CSS", 
				"PHP & MySQL", 
				"CMS Joomla", 
				"CMS TYPO3", 
				"CMS Wordpress"
				);
//Ausgabe: per print_r
echo "<pre>";
print_r ($cmsod);
// var_dump ($_SERVER);
echo "</pre>";
//Ausgabe: per foreach 
foreach ($cmsod as $value) {
    echo "Wert: $value <br>\n";
}
// echo $cmsod; // Fehler, weil kein "TEXT"
?>