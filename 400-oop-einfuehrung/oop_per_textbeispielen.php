<!doctype html public "-//W3C//DTD HTML 4.0 //EN"> 
<html> <head> <title>OOP-Konzepte bei PHP4/5</title> </head>
<body>

<?php
class TestKlasse {
    // Eigenschaften (Variablen)
	var $col="#FF0000"; 
	var $txt="Hallo Joe B."; 
	var $ft="Arial";
	// Methode (Funktion) - Hier: Konstruktor!!
    function __construct() {
      echo "<font face=\"$this->ft\" color=\"$this->col\">
                     $this->txt  </font><br>";
    }
}

class Einzeller {                // Klasse Einzeller
    function __construct(){      // Konstruktor
        echo "Einzeller wird initialisert<br>";
        }
    function vermehre($wert){    // Methode vermehre
        $wert++;
        return($wert);
        }
}

class Mehrzeller extends Einzeller {          // Klasse Mehrzeller
    function __construct(){                   // Konstruktor
        Einzeller::__construct();             // Statische Methode - NEUE M�GLICHKEIT ab PHP4
        echo "Mehrzeller wird initialisiert<br>";
        }
    function vermehre($wert) {
        $wert = Einzeller::vermehre($wert);   // Statische Methoden
        $wert = $wert * $wert;
        return($wert);
        }
}

$Wesen1 = new Einzeller;                   // neues Objekt / neue Instanz
echo $Wesen1->vermehre(10);
echo "<br><br>";
$Wesen2 = new Mehrzeller;                  // neues Objekt / neue Instanz
echo $Wesen2->vermehre(10);
// echo Einzeller::vermehre(10);


echo "<p>Informationen &uuml;ber die Klassen mit get_class, get_parent_class
und get_class_methods:</p>";
echo "Klasse: ";
echo get_class($Wesen2);                   // Klasse f�r Wesen2

echo "<br>Elternklasse: ";                 // Elternklasse f�r Wesen2
echo get_parent_class($Wesen2);
echo "<br>";
$class="Mehrzeller";
echo "<br><br><u>Methoden der Klasse $class:</u>";
$arr = get_class_methods($class);          // Methoden der Klasse Mehrzeller
foreach ($arr as $key=>$elem) {
    echo "<br>$key=>$elem";
    }

echo "<p>Objekte lassen sich wie assoziative Arrays (HASH) ansprechen:</p>";
$obj = new TestKlasse;
reset($obj);  // setzt Zeiger auf das erste Element

echo gettype($obj);
echo "<br>";
echo "<pre>";
print_r($obj);
echo "</pre>";
echo "Die Schriftfarbe ist $obj->col<br>";

foreach ($obj as $key=>$elem) {
    echo "$key => $elem <br>";
    }
?>

</body>
</html>



