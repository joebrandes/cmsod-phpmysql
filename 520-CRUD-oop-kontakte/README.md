#   Skripte für Kontakteverwaltung

Praxisbeispiel für die Kontakteverwaltung aus **Internet Magazin 2008 - Autor: C. Wenz**

Wird in Seminaren gern zum Ende für die Anwendung von PHP-Technik auf eigene Umsetzungen gezeigt.

Ich habe hier *alte* und *moderne* Techniken aufbereitet

##  Ordner mit alten und neuen Techniken

*   Originalbeispiel aus 2008 inklusive PDF mit Erläuterungen

*   Moderne Lösung mit OOP und Prepared Statements
