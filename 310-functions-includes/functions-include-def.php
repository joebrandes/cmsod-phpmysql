﻿<?php
function arrayAusgabe ($arr) {
	// Ausgabe mit print_r
	echo "<pre>";
	print_r ($arr);
	echo "</pre>";
}

function quadriereWert ($val) {
	// Quadrat berechnen:
	// global $quadrat;
	$quadrat = $val * $val;
	return $quadrat;
	// gerne auch mit Globalem Array:
	// $GLOBALS['quadrat'] = $val * $val;
	// return $GLOBALS['quadrat'];
}

?>