<?php
	require_once 'db.inc.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<title>Neu - Kontaktverwaltung</title>
</head>

<body>
<?php
// Formular validieren: (hier sehr einfach / grob)
// die Variablen vorname, nachname, email müssen
// alle per Methode Post übergeben worden sein und
// alle den Datentyp String besitzen und die email darf nicht leer sein!
if (isset($_POST['vorname']) && is_string($_POST['vorname']) && 
    isset($_POST['nachname']) && is_string($_POST['nachname']) && 
    isset($_POST['email']) && is_string($_POST['email']) && !empty($_POST['email'])) {
    
	// wieder so eine try/catch Konstruktion zur Fehler-/Ausnahmebehandlung
	try {
		// Prepared Statement: Einfügen-SQL-String mit Platzhaltern (?)
		$stmt = $mysqli->prepare('INSERT INTO kontakt (vorname, nachname, email) VALUES (?, ?, ?)');
		// im Prepared Statement Objekt die übergebenen POST-Inhalte binden
		$stmt->bind_param('sss', $_POST['vorname'], $_POST['nachname'], $_POST['email']);
		// SQL Einfügen ausführen in IF-Bedingung, also: bei Erfolg
		if ($stmt->execute()) {
			// spezielle Methode des DB-Konnektions-Objekts nutzen
			// Methode insert_id() ermittelt uns die neu erzeugte id des Datensatz
			$id = $mysqli->insert_id;
			// Infos zu neuem Datensatz (id) ausgeben, falls alles funktioniert hat
			echo 'Kontakt eingetragen (ID: ' . $id . ')!';
			} else {
				echo 'Fehler: ' . htmlspecialchars($db->error) . '!</td></tr>';
			}
			
		// alle Objekte wieder freigeben	
		$stmt->close();
		$mysqli->close();
		
		// hier wieder der catch-Teil zur try/catch Technik
		} catch (Exception $ex) {
			echo 'Fehler!';
		}
	}	// es folgt ein leeres HTML-Formular für neue Datensätze / Kontakte
?>
<form method="post" action="">
<table>
<thead>
	<tr> <th>Information</th> <th>Wert</th> </tr>
</thead>
<tbody>
	<tr>
		<td>Vorname</td> <td><input type="text" name="vorname" /></td>
	</tr>
	<tr>
		<td>Nachname</td> <td><input type="text" name="nachname" /></td>
	</tr>
	<tr>
		<td>E-Mail</td> <td><input type="text" name="email" /></td>
	</tr>
</tbody>
</table>
	<input type="submit" value="Kontakt anlegen" />
</form>
<p> <a href="index.php">Zur Startseite</a> </p>
</body>
</html>