<?php
	// Einfügen der Datenbank-Konnektion (und ggf. Helfer-Klassen)
	// Objekt für DB: mysqli (benannt wie die PHP-MySQLi-Klasse mysqli
	require_once 'db.inc.php';
?>
<!DOCTYPE HTML>
<html lang="de-DE">
<head>
	<meta charset="UTF-8">
	<title>Kontakteverwaltung mit OOP (Prepared Statements)</title>
</head>

<body>
<table style="width: 500px;">
	<thead> <!-- Tabellenkopf für Ausgabe Kontakte -->
		<tr>
			<th>titleID</th> <th>title</th> <th>publisherID</th> <th>year</th><th>Aktionen:</th>
		</tr>
	</thead>
<tbody>
<?php
try {	// Kapselungen in try/catch-Blöcke zur erweiterten Fehlerbehandlung
// Nutzen von MySQLi-Prepared-Statements Technik (Klasse: mysqli_stmt)
// SQL-SELECT vorbereiten - sortieren nach id - aufsteigend
if ($stmt = $mysqli->prepare("SELECT titleID, title, publisherID, year FROM title ORDER BY titleID ASC")) {
	// Abfrage (Query) durchführen
	$stmt->execute();
	// Ergebnisse (Spalten) an eigene / definierte Variablen binden
	$stmt->bind_result($titleID, $title, $publisherID, $year); 
  
	// Mit Methode fetch() die Ergebnisse in Schleife durchlaufen / ausgeben
	while($stmt->fetch()) {
		echo "<tr>\n";
		echo "<td><strong>" . $titleID . "</strong></td>"
          . "<td>" . htmlspecialchars($title) .  "</td>"
		  . "<td>" . htmlspecialchars($publisherID) .  "</td>"
		  . "<td>" . htmlspecialchars($year) .  "</td>" 
		  . "<td> <a href=\"bearbeiten.php?id=" . (int)$titleID . "\">bearbeiten</a>"
		  . "| <a href=\"loeschen.php?id=" . (int)$titleID . "\">löschen</a> </td>\n";
		echo "</tr>\n";
	}	// end while
	// Prepared Statement Objekt wieder freigeben
	$stmt->close();
}	// Ende der IF-Then-Anweisungen

	// Datenbankverbindungsobjekt freigeben
	$mysqli->close();
  
	// Fehler mit try/catch abfangen/ausgeben
	} catch (Exception $ex) {
		echo '<tr><td colspan="5">Fehler!</td></tr>';
	}
?>
	</tbody>
</table>

<p>
	<a href="neu.php">Neuen Kontakt anlegen</a>
</p>
</body>
</html>