<?php
  require_once 'db.inc.php';
?>
<!DOCTYPE HTML>
<html lang="de-DE">
<head>
<meta charset="UTF-8">
<title>Kontaktverwaltung</title>
<body>
<table>
<thead>
<tr><th>Nr.</th><th>Vorname</th><th>Nachname</th><th>E-Mail</th><th>&nbsp;</th></tr>
</thead>
<tbody>
<?php
  try {
    $db = new Singleton();
    $db = Singleton::holeVerbindung();
    //$db->holeVerbindung();
    $db->select_db('kontakte');
	
	
    if ($ergebnis = $db->query('SELECT * FROM kontakt')) {
      while ($zeile = $ergebnis->fetch_object()) {
	  
		//echo '<pre>'; print_r($zeile); echo '</pre>'; die ('Ende');
		
        printf('<tr><td>%s</td><td>%s</td><td>%s</td><td><a href="mailto:%s">%s</a></td><td><a href="bearbeiten.php?id=%s">Bearbeiten</a>&nbsp;<a href="loeschen.php?id=%s">L&ouml;schen</a></td></tr>',
          (int)$zeile->id,
          htmlspecialchars($zeile->vorname),
          htmlspecialchars($zeile->nachname),
          htmlspecialchars($zeile->email),
          htmlspecialchars($zeile->email),
          (int)$zeile->id,
          (int)$zeile->id);
      }
    } 
	
	
	
	else {
        echo '<tr><td colspan="5">Fehler: ' . 
          htmlspecialchars($db->error) .
          '!</td></tr>';
    }
	
	
	
	
    $db->close();
  } catch (Exception $ex) {
    echo '<tr><td colspan="5">Fehler!</td></tr>';
  }
?>
</tbody>
</table>
<p><a href="neu.php">Neuen Kontakt anlegen</a></p>
</body>
</html>