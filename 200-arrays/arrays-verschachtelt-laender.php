<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	
<?php
// ein verschachteltes Array für Länder
$laender = array ( "Deutschland" => array ("Berlin", "Euro", "deutsch"),
				   "Japan"       => array ("Tokio", "Yen", "japanisch"),
				   "Frankreich"  => array ("Paris", "Euro", "französisch") );
				   
echo $laender["Deutschland"][1]; echo "<br>";
echo $laender["Frankreich"][2]; echo "<br>";

foreach ($laender as $land => $landinfos) {
		list ($hauptstadt, $waehrung, $sprache) = $landinfos;
		echo "$land $hauptstadt $waehrung $sprache <br>\n";
}

?>


</body>
</html>
