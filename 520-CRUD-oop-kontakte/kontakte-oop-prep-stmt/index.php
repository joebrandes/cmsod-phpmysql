<?php
	// Einfügen der Datenbank-Konnektion (und ggf. Helfer-Klassen)
	// Objekt für DB: mysqli (benannt wie die PHP-MySQLi-Klasse mysqli
	require_once 'db.inc.php';
?>
<!DOCTYPE HTML>
<html lang="de-DE">
<head>
	<meta charset="UTF-8">
	<title>Kontakteverwaltung mit OOP (Prepared Statements)</title>
</head>

<body>
<table style="width: 500px;">
	<thead> <!-- Tabellenkopf für Ausgabe Kontakte -->
		<tr>
			<th>Nr.</th> <th>Vorname</th> <th>Nachname</th> <th>E-Mail</th> <th>Aktionen</th>
		</tr>
	</thead>
<tbody>
<?php
try {	// Kapselungen in try/catch-Blöcke zur erweiterten Fehlerbehandlung
// Nutzen von MySQLi-Prepared-Statements Technik (Klasse: mysqli_stmt)
// SQL-SELECT vorbereiten - sortieren nach id - aufsteigend
if ($stmt = $mysqli->prepare("SELECT id, vorname, nachname, email FROM kontakt ORDER BY id ASC")) {
	// Abfrage (Query) durchführen
	$stmt->execute();
	// Ergebnisse (Spalten) an eigene / definierte Variablen binden
	$stmt->bind_result($id, $vorname, $nachname, $email); 
  
	// Mit Methode fetch() die Ergebnisse in Schleife durchlaufen / ausgeben
	while($stmt->fetch()) {
		echo "<tr>\n";
		echo "<td><strong>" . $id . "</strong></td>"
          . "<td>" . htmlspecialchars($vorname) .  "</td>"
		  . "<td>" . htmlspecialchars($nachname) .  "</td>"
		  . "<td>" . htmlspecialchars($email) .  "</td>" 
		  . "<td> <a href=\"bearbeiten.php?id=" . (int)$id . "\">bearbeiten</a>"
		  . "| <a href=\"loeschen.php?id=" . (int)$id . "\">löschen</a> </td>\n";
		echo "</tr>\n";
	}	// end while
	// Prepared Statement Objekt wieder freigeben
	$stmt->close();
}	// Ende der IF-Then-Anweisungen

	// Datenbankverbindungsobjekt freigeben
	$mysqli->close();
  
	// Fehler mit try/catch abfangen/ausgeben
	} catch (Exception $ex) {
		echo '<tr><td colspan="5">Fehler!</td></tr>';
	}
?>
	</tbody>
</table>

<p>
	<a href="neu.php">Neuen Kontakt anlegen</a>
</p>
</body>
</html>