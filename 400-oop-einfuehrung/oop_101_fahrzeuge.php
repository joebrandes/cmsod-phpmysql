<?php
// Kommentar imm Seminar für das Zeigen mit Git...
class fahrzeug {
    // Eigenschaften (Variblen) - Standard für Sichtbarkeit: public
    public $farbe = "rot";
    public $gewicht = "1200 kg";
    // Konstruktor
    public function __construct() {
        echo "<p>Hallo Fahrzeuginstanz!</p>";
    }
    // Methode: bremse()
    public function bremse($a) {
        echo "<p>Ich bremse mit Kraft $a</p>";
    }
    // Methode: beschleunige()
    public function beschleunige($a) {
        echo "<p>Ich beschleunige mit Kraft $a</p>";
    }
}

// Klasse suv erbt von fahrzeug
class suv extends fahrzeug {
    public $farbe      = "blau";
    public $sitzanzahl = 6;
}

// Neue Instanz/Objekt erzeugen von Klasse Fahrzeug
$fz = new fahrzeug;
$fz->beschleunige(50);
$fz->bremse(20);
// Neue Instanz/Objekt erzeugen von Klasse SUV
$fz2 = new suv;  // Beachte Konstruktor!
echo "<p>Farbe SUV: $fz2->farbe </p>";
echo "<p>Sitzanzahl SUV: $fz2->sitzanzahl </p>";
echo "<p>Gewicht SUV: $fz2->gewicht </p>";
$fz2->beschleunige(10);
// Statische Methode aufrufen: (s.o static vor/statt public)
// fahrzeug::beschleunige(5);

?>
