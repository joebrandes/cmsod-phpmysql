<?php
/* Beispiel 1 */

for ($i = 1; $i <= 10; $i++) {
    echo $i;
}

/* Beispiel 2 */
echo "<p>";
for ($i = 1; ; $i++) {
    if ($i > 10) {
        break;
    }
    echo $i, "<br>";
}
echo "</p>";

