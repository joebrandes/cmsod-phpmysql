<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	
	<style>
	table {width: 50%;}
	table, tr, td {
		border: 1px solid #000000; 
		border-collapse: collapse;
	}
	</style>
	
</head>
<body>

<?php
// Definition meines nummerischen Arrays für die Orte:
$orte = array("Trier", "Koblenztest", "Mainz");
// ein Element des Arrays ausgeben:
echo $orte[1] , "<br />\n";
// Anzahl der Elemente im Array ermitteln:
$anzahl = count($orte);
echo $anzahl . "<br />\n";
// ein neues Element in Array aufnehmen:
$orte[] = "Köln";
$orte[] = 5;
echo $orte[3] . "<br />\n";

// Anzeige / Übersicht meiner Arrays mit Testfunktionen:
/* echo "<pre>";
print_r($orte);
echo "</pre>";

echo "<pre>";
var_dump($orte);
echo "</pre> <br />\n";
*/

// verbesserte Ausgabe von Arrays mit foreach Tabellen
// hier natürlich ohne CSS!!!!
echo "<div> <table>";
foreach ($orte as $key => $value) {
	echo "<tr class=\"row\">
			<td class=\"col\">$key</td>
			<td class=\"col\">$value</td>
		</tr>";
}
echo "</table> </div>";


// einfache Ausgabe von Arrays mit foreach
foreach ($orte as $key => $value) {
	echo $key , " mit Wert ", $value, "<br />\n";
}

// ein assoziatives Array (Hash)
$kontakt = array (  "Vorname"  => "Max",
					"Nachname" => "Mustermann",
					"PLZ"      => "55000",
					"Ort"      => "Musterhausen" );

// verbesserte Ausgabe von Arrays mit foreach Tabellen
echo "<div> <table>";
foreach ($kontakt as $key => $value) {
	echo "<tr class=\"klasse1 klasse2 subklasse3\">
			<td class=\"col\">$key</td>
			<td class=\"col\">$value</td>
		</tr>";
}
echo "</table> </div>";

/* echo "<pre>";
print_r($kontakt);
echo "</pre>";

echo "<pre>";
var_dump($kontakt);
echo "</pre>";
*/			
?>

</body>
</html>









