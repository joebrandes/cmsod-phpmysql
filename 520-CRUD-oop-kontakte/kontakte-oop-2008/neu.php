<?php
  require_once 'db.inc.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>Kontaktverwaltung</title>
<body>
<?php
  if (isset($_POST['vorname']) && is_string($_POST['vorname']) && 
      isset($_POST['nachname']) && is_string($_POST['nachname']) && 
      isset($_POST['email']) && is_string($_POST['email'])) {
    try {
      $db = Singleton::holeVerbindung();
      $db->select_db('kontakte');
      $sql = $db->prepare('INSERT INTO kontakt (vorname, nachname, email) VALUES (?, ?, ?)');
      $sql->bind_param('sss', $_POST['vorname'], $_POST['nachname'], $_POST['email']);
      if ($sql->execute()) {
        $id = $db->insert_id;
        echo 'Kontakt eingetragen (ID: ' . $id . ')!';
      } else {
          echo 'Fehler: ' . htmlspecialchars($db->error) . '!</td></tr>';
      }
      $db->close();
    } catch (Exception $ex) {
      echo 'Fehler!';
    }
  }
?>
<form method="post" action="">
<table>
<thead>
<tr><th>Information</th><th>Wert</th></tr>
</thead>
<tbody>
<tr><td>Vorname</td><td><input type="text" name="vorname" /></td></tr>
<tr><td>Nachname</td><td><input type="text" name="nachname" /></td></tr>
<tr><td>E-Mail</td><td><input type="text" name="email" /></td></tr>
</tbody>
</table>
<input type="submit" value="Kontakt anlegen" />
</form>
<p><a href="index.php">Zur Startseite</a></p>
</body>
</html>