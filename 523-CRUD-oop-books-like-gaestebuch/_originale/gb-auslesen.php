<html>
<head>
  <title>G&auml;stebuch</title>
</head>
<body>
<h1>G&auml;stebuch</h1>
<?php
  if ($db = mysqli_connect("localhost", "root", "", "PHP")) {
    $sql = "SELECT * FROM gaestebuch ORDER BY datum DESC";
    $ergebnis = mysqli_query($db, $sql);
	
	// print_r ($ergebnis); die ("Tschüss");
	
    while ($zeile = mysqli_fetch_object($ergebnis)) {
	
	  // print_r ($zeile); die ("Tschüss");
		
      printf("<p><a href=\"mailto:%s\">%s</a> schrieb am/um %s:</p>
        <h3>%s</h3><p>%s</p><hr noshade=\"noshade\" />", 
        urlencode($zeile->email),
        htmlspecialchars($zeile->autor),
        htmlspecialchars(date("d.m.Y, H:i", strtotime($zeile->datum))),
        htmlspecialchars($zeile->ueberschrift),
        nl2br(htmlspecialchars($zeile->eintrag))
      );
    }
    mysqli_close($db);
  } else {
    echo "Fehler: " . mysqli_connect_error() . "!";
  }
?>
</body>
</html>
