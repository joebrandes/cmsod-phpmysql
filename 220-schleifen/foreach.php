<?php
$arr = array(1, 2, 3, 4);
foreach ($arr as &$value) {
    $value = $value * 2;
}
// $arr ist jetzt array(2, 4, 6, 8)

// ohne ein unset($value), bleibt $value weiterhin eine Referenz auf das letzte Element: $arr[3]

foreach ($arr as $key => $value) {
    // $arr[3] wird mit jedem Wert von $arr aktualisiert...
    echo "{$key} => {$value} <br>";
    echo "<pre>"; print_r($arr); echo "</pre>";
}
// bis schließlich der vorletzte Wert in den letzten Wert kopiert wird

// Ausgabe:
// 0 => 2 Array ( [0] => 2, [1] => 4, [2] => 6, [3] => 2 )
// 1 => 4 Array ( [0] => 2, [1] => 4, [2] => 6, [3] => 4 )
// 2 => 6 Array ( [0] => 2, [1] => 4, [2] => 6, [3] => 6 )
// 3 => 6 Array ( [0] => 2, [1] => 4, [2] => 6, [3] => 6 )
?> 