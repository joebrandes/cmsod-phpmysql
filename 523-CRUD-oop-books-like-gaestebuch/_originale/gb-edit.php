<html>
<head>
  <title>G&auml;stebuch</title>
</head>
<body>
<h1>G&auml;stebuch</h1>
<?php
  $Name = "";
  $Email = "";
  $Ueberschrift = "";
  $Kommentar = "";

  if (isset($_GET["id"]) &&
       is_numeric($_GET["id"])) {
    if ($db = mysqli_connect("localhost", "root", "", "PHP")) {
      if (isset($_POST["Name"]) &&
          isset($_POST["Email"]) &&
          isset($_POST["Ueberschrift"]) &&
          isset($_POST["Kommentar"])) {
        $sql = "UPDATE gaestebuch SET 
          ueberschrift = ?, 
          eintrag = ?,
          autor = ?,
          email = ?
          WHERE id=?";
        $kommando = mysqli_prepare($db, $sql);
        mysqli_stmt_bind_param($kommando, "ssssi", 
          $_POST["Ueberschrift"], 
          $_POST["Kommentar"],
          $_POST["Name"], 
          $_POST["Email"],
          intval($_GET["id"]));
        if (mysqli_stmt_execute($kommando)) {
          echo "<p> Eintrag ge&auml;ndert.</p>
                <p><a href=\"gb-admin.php\">Zur&uuml;ck zur &Uuml;bersicht</a></p>";
        } else {
          echo "Fehler: " . mysqli_error($db) . "!";
        }
      }

      $sql = sprintf("SELECT * FROM gaestebuch WHERE id=%s", 
        mysqli_real_escape_string($db, $_GET["id"]));
      $ergebnis = mysqli_query($db, $sql);
      if ($zeile = mysqli_fetch_object($ergebnis)) {
        $Name = $zeile->autor;
        $Email = $zeile->email;
        $Ueberschrift = $zeile->ueberschrift;
        $Kommentar = $zeile->eintrag;
      }
      mysqli_close($db);
    } else {
      echo "Fehler: " . mysqli_connect_error() . "!";
    }
  }
?>
<form method="post">
Name <input type="text" name="Name" value="<?php
  echo htmlspecialchars($Name);
?>" /><br />
E-Mail-Adresse <input type="text" name="Email" value="<?php
  echo htmlspecialchars($Email);
?>" /><br />
&Uuml;berschrift <input type="text" name="Ueberschrift" value="<?php
  echo htmlspecialchars($Ueberschrift);
?>" /><br />
Kommentar
<textarea cols="70" rows="10" name="Kommentar"><?php
  echo htmlspecialchars($Kommentar);
?></textarea><br />
<input type="submit" name="Submit" value="Aktualisieren" />
</form>
</body>
</html>
