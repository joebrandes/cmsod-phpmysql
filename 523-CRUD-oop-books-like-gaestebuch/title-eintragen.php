<html>
<head>
  <title>Books</title>
</head>
<body>
<h1>Books eintragen:</h1>
<?php
  if (isset($_POST["Titel"]) && !empty($_POST["Titel"]) &&
      isset($_POST["VerlagsID"]) && !empty($_POST["VerlagsID"]) &&
      isset($_POST["Jahrgang"]) && !empty($_POST["Jahrgang"]) )
	  {
    if ($db = mysqli_connect("localhost", "root", "", "books2")) {
      $sql = "INSERT INTO title
        (title,
         publisherID,
         year)
        VALUES (?, ?, ?)";
      $kommando = mysqli_prepare($db, $sql);
      mysqli_stmt_bind_param($kommando, "sii",
        $_POST["Titel"],
        $_POST["VerlagsID"],
        $_POST["Jahrgang"]);
      if (mysqli_stmt_execute($kommando)) {
        $id = mysqli_insert_id($db);
        echo "Eintrag hinzugef&uuml;gt.";
      } else {
        echo "Fehler: " . mysqli_error($db) . "!";
      }
      mysqli_close($db);
    } else {
      echo "Fehler: " . mysqli_connect_error() . "!";
    }
  }
?>
<form method="post">
Titel <input type="text" name="Titel" /><br />
Publisher ID <input type="text" name="VerlagsID" /><br />
Jahr <input type="text" name="Jahrgang" /><br />
<input type="submit" name="Submit" value="Eintragen" />
</form>
</body>
</html>
