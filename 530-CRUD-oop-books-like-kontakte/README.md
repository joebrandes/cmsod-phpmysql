#   Skripte für DB Books per COPY and PASTE

Praxisbeispiel für die Adaption der **Kontakte OOP mit Prepared Statements**
Skripte für die **Bücher-Datenbank books**

Wird in Seminaren gern zum Ende für die Anwendung von PHP-Technik auf eigene Umsetzungen gezeigt.

Im Grunde handelt es sich um *Copy and Paste Lösungen*!

##  Übungen

*   Komplettierung der Codes für CRUD

*   Code *säubern* und verfeinern
