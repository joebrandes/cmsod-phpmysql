<?php 
	$variable = "Irgendein Text";
	$variable2 = "Noch nen Text";
	$zahl = 5;
	$zahl2 = 10.4;
	// mit . bei Echo Zeichenketten "addieren"
	// Alternative: mit Komma aneinanderhängen / auflisten
	echo $variable . "<br>";
	// Kommentar: Testen Sie mal einfache Anf.-Zeichen
	echo "Der Wert ist $zahl";
	echo "<br>";	
	/* hier jetzt
	 * ein mehrzeiliger
	 * Kommentar
	*/
	echo PHP_VERSION; echo "<br>";
	echo __LINE__; echo "<br>";
	// "Echte" eigene Konstanten definieren
	define ("CONSTANT", "Hallo Welt!");
	echo CONSTANT;   echo "<br>"; // Groß/Kleinschreibung testen
	// phpinfo(INFO_CONFIGURATION);
	// PHP-Funktion für Datum/Uhrzeit: date()
	$heute = date("d.m.y");
	echo "Heute ist: $heute";  echo "<br>";
	// geplante Datumanzeige:
	// 2016-04-25 13:35:05   
	$heute2 = date("Y-m-d H:i:s");
	echo $heute2;  echo "<br>";
	// Arrays
	// hier: Superglobale Arrays
	// z.B. für den "Server":
	// $_SERVER[  ]
	echo $_SERVER['SERVER_NAME']; echo "<br>";
	echo $_SERVER['DOCUMENT_ROOT']; echo "<br>";
	echo $_SERVER['PHP_SELF']; echo "<br>";
?>