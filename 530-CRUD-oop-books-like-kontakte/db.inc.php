<?php
// DB-Konnection und Fehlerbehandlung für Datenbankserver / Datenbank
$mysqli = new mysqli("localhost", "root", "", "books");
if ($mysqli->connect_error) {
  echo "Fehler bei der Verbindung: " . mysqli_connect_error();
  exit();
}
if (!$mysqli->set_charset("utf8")) {
  echo "Fehler beim Laden von UTF8 ". $mysqli->error;
}
?>