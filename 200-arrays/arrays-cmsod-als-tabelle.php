<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	
<?php
// mehrdimensionales Array für CMSOD-Infos
$cmsod = array (  "Modul I" => array ( "HTML & CSS", "statisch"),
					"Modul II"       => array ( "PHP & MySQL", "dynamisch"),					
					"Modul IIIa"     => array ( "Joomla", "CMS"),
					"Modul IIIb"  => array ( "TYPO3", "CMS")					
					);
// TESTING-Routine: Ausgabe des Arrays
//echo "<pre>";
//print_r($cmsod);
//echo "</pre>"; die ("hier ist jetzt mal Ende");

echo "<div> <table border=1 width=75%>";
echo "<tr>
    <th>Module</th>
    <th>Modulnamen</th>
    <th>Anmerkungen</th>
  </tr>";			
// mit foreach die Infos ausgeben:
foreach ($cmsod as $modul => $modulinfos) {
	// mit Befehl list die Variablen hauptstadt und waehrung zuweisen:
	list ($modulname, $anmerkungen) = $modulinfos;

	echo "<tr class=\"row\">
			<td class=\"col\">$modul</td>
			<td class=\"col\">$modulname</td>
			<td class=\"col\">$anmerkungen</td>
		</tr>";
}
echo "</table> </div>";

// Werte aus verschachtelten Array definieren und ausgeben:
echo $cmsod['Modul II'][1];


?>
	
	
	
</body>
</html>