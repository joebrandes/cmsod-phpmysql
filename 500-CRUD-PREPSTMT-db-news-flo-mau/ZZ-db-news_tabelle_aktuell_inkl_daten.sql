-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 29. Jul 2021 um 19:02
-- Server-Version: 10.4.17-MariaDB
-- PHP-Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `news`
--
CREATE DATABASE IF NOT EXISTS `news` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `news`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aktuell`
--

CREATE TABLE `aktuell` (
  `id` int(11) NOT NULL,
  `titel` varchar(50) NOT NULL,
  `text` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `aktuell`
--

INSERT INTO `aktuell` (`id`, `titel`, `text`) VALUES
(1, 'Sommerloch ...', 'Ein Teil des Sommerlochphänomens ist der Bericht über Sommerlöcher vergangener Sommer.'),
(2, 'Oktoberfest im September', 'Gleichzeitig mit dem diesjährigen Oktoberfest findet die Landwirtschaftausstellung statt ... usw. usf.'),
(3, 'Regentage', 'Wohl dem, der Gummistiefel hat. Und besonders dem, der über Matsch- und Buddelhose verfügt.');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `aktuell`
--
ALTER TABLE `aktuell`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `aktuell`
--
ALTER TABLE `aktuell`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
