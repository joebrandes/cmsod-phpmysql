# PHP & MySQL

Unterlagen Trainer Joe Brandes für **PHP & MySQL** Seminare

## Getting Started

Gerne einfach das Repo klonen:

```
git clone https://gitlab.com/joebrandes/cmsod-phpmysql.git
```

This - obviously - creates a new folder *cmsod-phpmysql* and and the rest of the git-structure as subfolder

## Links

* [Joe Brandes](http://www.joe-brandes.de) - Web *Card* by Joe Brandes
* [Git](https://www.git-scm.com/) - Git Development

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
