<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	
	<style>
	table {width: 75%;}
	table, tr, td {
		border: 1px solid #000000; 
		border-collapse: collapse;
	}
	td { text-align: center;}
	</style>
	
</head>
<body>
<?php
/* Beispiel 1-mal-1 */
echo "<table>\n";
for ($i = 1; $i <= 10; $i++) {
	echo "<tr>\n";
    for ($j = 1; $j <= 10; $j++) {
	
		$produkt = $i *$j;
		echo "<td>$produkt</td>\n";

	}
	echo "</tr>";
}
echo "</table>";
?>

</body>
</html>