<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	
<?php
// mehrdimensionales Array für Länderinfos
$laender = array (  "Deutschland" => array ( "Berlin", "Euro", "deutsch"),
					"Japan"       => array ( "Tokio", "Yen", "japanisch"),					
					"England"     => array ( "London", "Pfund", "englisch"),
					"Frankreich"  => array ( "Paris", "Euro", "franz."),					
					"Italien"     => array ( "Rom", "Euro", "italienisch")					
					);


echo "<div> <table border=1 width=50%>";
echo "<tr>
    <th>Land</th>
    <th>Hauptstadt</th>
    <th>Währung</th>
	<th>Sprache</th>
  </tr>";			
// mit foreach die Infos ausgeben:
foreach ($laender as $country => $countryinfos) {
	// mit Befehl list die Variablen hauptstadt und waehrung zuweisen:
	list ($hauptstadt, $waehrung, $sprache) = $countryinfos;
	// einfache Ausgabe von LAND, hauptstadt, waehrung:
	// echo $country, " ", $hauptstadt, " ", $waehrung, "<br />\n";
	echo "<tr class=\"row\">
			<td class=\"col\">$country</td>
			<td class=\"col\">$hauptstadt</td>
			<td class=\"col\">$waehrung</td>
			<td class=\"col\">$sprache</td>
		</tr>";
}
echo "</table> </div>";

// Werte aus verschachtelten Array definieren und ausgeben:
echo $laender['Deutschland'][1];


?>
	
	
	
</body>
</html>