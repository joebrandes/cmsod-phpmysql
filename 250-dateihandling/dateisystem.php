﻿<!doctype html>
<html lang="de">
<head>
	<meta charset="UTF-8">
	<title>Handeln des Dateisystems</title>
</head>
<body>

<?php
$path = "daten.txt";
$modus = "r";
if ($fp = fopen($path, $modus)) {
    echo "die Datei <b>$path</b> wurde geöffnet. Daten gelesen.<br>";
    while ($str = fgets($fp, 100))  {
        echo $str, "<br>";
        }
    }
else
    echo "die Datei konnte nicht ge&ouml;ffnet werden!";
    
if (fclose($fp))
   echo "<br> und wieder geschlossen";

$path = "schreiben.txt";
$modus = "a+";
$str = "Das ist ein Dummytext, der einfach die ganze Zeit wiederholt wird.
Das ist ein Dummytext, der einfach die ganze Zeit wiederholt wird.
Das ist ein Dummytext, der einfach die ganze Zeit wiederholt wird.
Das ist ein Dummytext, der einfach die ganze Zeit wiederholt wird.
";

if ($fp=fopen($path, $modus)) {
    fwrite($fp, $str);
    print "<p>Es wurde eine Textdatei <b>$path</b> geschrieben!";
    }
else
    echo "Die Datei konnte nicht geschrieben werden!";
fclose ($fp);

echo "<p>Verzeichnis-Funktionen:</p>";
$path = ".";
$folder = dir($path);                // Verzeichnisklasse dir
$free = diskfreespace($path);
echo "<b>Inhalt des Verzeichnisses ", $path, "</b><br>";
echo $free, " Bytes frei<br>";
while ($datei = $folder->read()) {   // Methode read
    echo "\n$datei<br>";
    }
$folder->close();                    // Methode close


// mkdir("test",0700);                  // Anlegen eines Verzeichnisse test
// rmdir("test");
?>

</body>
</html>
