<?php
/* Beispiel 1 */

$i = 1;
while ($i <= 10) {
    echo $i++;  /* der ausgegebene Wert ist $i bevor
                   er erhöht wird (post-increment) */
}

/* Beispiel 2 */

$i = 1;
while ($i <= 10):
    echo $i;
    $i++;
endwhile;
?> 