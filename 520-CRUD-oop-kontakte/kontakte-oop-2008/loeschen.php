<?php
  require_once 'db.inc.php';
?>
<html>
<head>
<title>Kontaktverwaltung</title>
<body>
<?php
  if (!isset($_GET['id'])) {
    header('Location: index.php');
  } elseif (!isset($_POST['ok'])) {
    $id = (int)$_GET['id'];
    try {
      $db = Singleton::holeVerbindung();
      $db->select_db('kontakte');
      if (($ergebnis = $db->query('SELECT * FROM kontakt WHERE id=' . $id)) &&
          $zeile = $ergebnis->fetch_object()) {
        printf('Wollen Sie wirklich den Datensatz Nr. %s (%s %s, %s) l&ouml;schen?<br />' . 
               '<form method="post" action=""><input type="hidden" name="ok" value="true" /><input type="submit" value="L&ouml;schen" /></form>', 
               $id, 
               htmlspecialchars($zeile->vorname), 
               htmlspecialchars($zeile->nachname), 
               htmlspecialchars($zeile->email));
      } else {
      echo 'Fehler: ' . htmlspecialchars($db->error) . '!</td></tr>';
      }
      $db->close();
    } catch (Exception $ex) {
      echo 'Fehler!';
    }
  } else {
    $id = (int)$_GET['id'];
    try {
      $db = Singleton::holeVerbindung();
      $db->select_db('kontakte');
      if ($ergebnis = $db->query('DELETE FROM kontakt WHERE id=' . $id)) {
        echo 'Datensatz gel&ouml;scht!';
      } else {
      echo 'Fehler: ' . htmlspecialchars($db->error) . '!</td></tr>';
      }
    } catch (Exception $ex) {
      echo 'Fehler!';
    }
  }
?>
<p><a href="index.php">Zur Startseite</a></p>
</body>
</html>