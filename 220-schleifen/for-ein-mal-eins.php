<?php
/* Beispiel 1 
   direkt aus PHP-Hilfe bzw. Handbuch kopiert */
/*
for ($i = 1; $i <= 10; $i++) {
    echo $i, " ";
}

echo "<br>";
*/
echo "<table border=\"1\">";

for ($i = 1; $i <= 10; $i++) {
	echo "<tr class=\"klasse4reihe\">";
	for ($j = 1; $j <= 10; $j++) {
		$ergebnis = $i * $j;
		echo "<td>$ergebnis</td>";
	}
	echo "</tr>";
}

echo "</table>";


?>